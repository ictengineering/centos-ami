#!/bin/bash

curl -LO https://go.dev/dl/go1.18.2.linux-amd64.tar.gz
tar -xf go1.18.2.linux-amd64.tar.gz -C /home/ec2-user/.local/opt
rm go1.18.2.linux-amd64.tar.gz
echo 'export GOLANG_VERSION=1.18.2' >> /home/ec2-user/.bashrc
echo 'export GOPATH=/home/ec2-user/.local/go' >> /home/ec2-user/.bashrc

