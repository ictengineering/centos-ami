#!/bin/bash

packages=(
  packer
  terraform
)

stages=( 
  "scripts/pkr/enable-epel-sshd.sh" 
  "scripts/pkr/install-dev-tools.sh" 
  "scripts/pkr/install-aws-cli.sh" 
  "scripts/pkr/customize-bash.sh"
)

base_ami=""
ami_name=""
ami_manifest=""
ami_list=()

[[ ${1} ]] && base_ami=${1} || base_ami="ami-037b3bac625d73b1f" # default centos 9 stream x86_64 ireland

RED='\033[1;31m'
BLUE='\033[1;36m'
NOCOLOR='\033[0;0m'

function Welcome() {
  echo -e "\n\tIncremental packer build and cleanup with terraform\n\n\t${BLUE}START${NOCOLOR}\n\n"
  echo -e "\n\t${BLUE}PACKAGES: ${packages[@]}\n\tSTAGES: ${stages[@]}${NOCOLOR}\n\n"
}

function EnsureDependenciesInstalled() {
  errors=()
  for i in ${!packages[@]}; do
    [[ ! $(whereis ${packages[i]} | cut -d':' -f2 | wc -w) -gt 0 ]] && errors+=("${packages[i]} not installed")
  done

  if [[ ${#errors[@]} -gt 0 ]]; then
    for i in ${!errors[@]}; do
      printf "${RED}${errors[$i]}${NOCOLOR}\n"
    done
    exit 1 
  fi
}

function PackerStandardBuild() {
  output=$(packer build \
        -machine-readable \
        -var "ami_name=${ami_name}" \
        -var "base_ami=${base_ami}" \
        -var "vpc_id=${PACKER_VPC_ID}" \
        -var "subnet_id=${PACKER_SUBNET_ID}" \
        -var "ssh_username=${PACKER_SSH_USERNAME}" \
        -var "instance_size=${PACKER_INSTANCE_SIZE}" \
        -var "description=${PACKER_DESCRIPTION}" \
        -var "manifest=${ami_manifest}" \
        ${PACKER_CONTEXT} 2>&1 | tee /dev/tty)

  if [[ ! $? -eq 0 ]]; then
     [[ $(echo ${output} | grep "Error") = *"existing"*"AMI"* ]] && echo -e "\n\t${BLUE}Image ${ami_name} already exists - skipping${NOCOLOR}\n" || return $?
  fi
}

function CheckImage() {
  timeout 30 aws ec2 wait image-exists --image-ids ${base_ami} && return 0 || return 1
}

function RemoveAmi() {
  for i in ${!ami_list[@]}; do
    id=$(echo ${ami_list[i]} | cut -d'=' -f1)
    snapshot=$(echo ${ami_list[i]} | cut -d'=' -f2)
    owner=$(echo ${ami_list[i]} | cut -d'=' -f3)
    me=$(aws sts get-caller-identity --query "Account" --output text)
    [[ $i -eq $((${#ami_list[@]} -1)) ]] && break
    if [[ ${owner} = ${me} ]]; then
      ( aws ec2 deregister-image --image-id ${id} && aws ec2 delete-snapshot --snapshot-id ${snapshot} ) || 
         { echo -e "\n\t${RED}Ami not Removed${NOCOLOR\n}"; exit 1; }
    else
      echo -e "\n\t${BLUE}Image ${id} is not mine${NOCOLOR}\n"  
    fi
  done
}

function AddToAmiList() {
  ami_description=$(aws ec2 describe-images --image-ids ${base_ami})
  owner=$(echo ${ami_description} | jq -r '.Images' | jq .[0] | jq -r '.OwnerId')
  snapshot=$(echo ${ami_description} | jq -r '.Images' | jq .[0] | jq -r '.BlockDeviceMappings' | jq .[0] | jq -r '.Ebs' | jq -r '.SnapshotId')
  ami_list+=("${base_ami}=${snapshot}=${owner}")
}

function PackerIncrementalBuild() {
  set -a
  . .env
  set +a
  # for i in ${!stages[@]}; do
    # echo -e "\n\t${BLUE}AMIs: ${ami_list[@]}${NOCOLOR}\n\n"
    # ami_name=$([[ "$i" -eq $((${#stages[@]} - 0)) ]] && echo "ami-centos9stream" || echo "ami-centos9stream-tmp-${i}")
    # [[ $(CheckImage) -eq 1 ]] && { echo -e "${RED}\n\t Image ${base_ami} doesn't exist\n"; exit 1; }
    # AddToAmiList
    ami_name="ami-centos9stream"
    ami_manifest="manifests/${ami_name}-manifest.json"
    PackerStandardBuild || continue
    # base_ami=$(cat ${ami_manifest} | jq -r '.builds' | jq .[0] | jq -r '.artifact_id' | cut -d':' -f2)
     
    # [[ ${#ami_list} -gt 1 ]] && RemoveAmi
  # done
}

function GoodBye() {
  echo -e "\n\n\t${BLUE}END${NOCOLOR}\n\n"
}

Welcome
EnsureDependenciesInstalled
PackerIncrementalBuild
GoodBye





