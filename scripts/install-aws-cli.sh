#!/bin/bash

curl 'https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip' -o 'awscliv2.zip'
unzip awscliv2.zip
rm awscliv2.zip
./aws/install -i /home/ec2-user/.local/opt/aws-cli -b /home/ec2-user/.local/bin
rm -rf ./aws
echo -e "\n\n\t\t_______________________AWS CLI INSTALLED____________________\n\n"

