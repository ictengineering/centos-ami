#!/bin/bash

wget -qO- https://micromamba.snakepit.net/api/micromamba/linux-64/latest | tar -xvj bin/micromamba
mv bin/micromamba /home/ec2-user/.local/bin/micromamba -v
/home/ec2-user/.local/bin/micromamba shell init --shell=bash --prefix=/home/ec2-user/.micromamba
eval '$(/home/ec2-user/.local/bin/micromamba shell hook --shell=bash)'

