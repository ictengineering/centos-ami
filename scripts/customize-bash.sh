#!/bin/bash

echo -e "shopt -s globstar\nshopt -s autocd\nshopt -s histappend\nshopt -s extglob\nHISTCONTROL=ignoredups:erasedups" >> /home/ec2-user/.bashrc
echo "export PATH=/home/ec2-user/.cargo/bin:/home/ec2-user/.local/opt/go/bin:/home/ec2-user/.local/bin:${PATH}" >> /home/ec2-user/.bashrc
[[ -f /etc/profile.d/bash_completion.sh ]] && echo 'source /etc/profile.d/bash_completion.sh' >> /home/ec2-user/.bashrc
# [[ $(which aws_completer 2> /dev/null) ]] && echo 'complete -C $(which aws_completer) aws' >> /home/ec2-user/.bashrc
# [[ $(which micromamba 2> /dev/null) ]] && echo -e 'micromamba activate' >> /home/ec2-user/.bashrc

