#!/bin/bash

sudo dnf update -y
sudo dnf install dnf-plugins-core -y
sudo dnf config-manager --set-enabled crb -y
sudo dnf install epel-release epel-next-release -y
sudo dnf update --refresh
sudo systemctl enable sshd

