#!/bin/bash

sudo dnf install bzip2 lzop unzip wget npm openssl-devel bash-completion gcc lldb git gdb g++ clang llvm cmake neovim podman buildah skopeo -y
mkdir -p /home/ec2-user/.local/{opt,bin}

